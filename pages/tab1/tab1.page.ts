import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  articles: Article[] = [];

  constructor(private data: NewsService) { }

  ngOnInit(): void {
    this.loadNews();
  }

  loadNews(event?) {

    (async () => {
      const response = await this.data.getNews();
      if (response.articles.length === 0) {
        event.target.disables = true;
        event.target.complete();
        return;
      }
      this.articles.push(...response.articles);
      if (event) {
        event.target.complete();
      }
    })();
  }

  loadData(event: any) {
    this.loadNews(event);
  }
}
